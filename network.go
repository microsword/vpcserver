package vpcserver

const (
	Register  = 0x01
	KeepAlive = 0x02
	Data      = 0x03
	CloseConn = 0x04
)

// AgentDto 数据传播结构体
type AgentDto struct {
	Action     byte   //指令类型
	ProxyId    string //代表本proxy的ID，唯一
	Ticket     string //本次隧道会话的票据，用于寻找集群内的引擎节点
	Token      string //握手token
	Success    bool   //握手是否成功
	AssetAddr  string //内网资产转发目标地址
	ClientAddr string //客户端来源连接地址
}

package vpcserver

import (
	"fmt"
	"testing"
)

func TestStart(t *testing.T) {
	fmt.Println("starting")
	config := &ServerConfig{
		BindIp:   "127.0.0.1",
		BindPort: 65000,
	}
	server := NewServer(config, reverse)
	ticket := server.newTicket(180)
	server.StartTLSServer("./cert/server.crt", "./cert/server.key")
	fmt.Println(ticket)
	select {}

}
func reverse(proxyId, ticket, token string) (bool, error) {

	return true, nil
}
